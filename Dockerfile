FROM debian:stretch

ARG PHP_VERSION=7.4
ARG PHP_VERSION_FULL=php7.4
ARG YARN_VERSION=1.19.2

COPY ./env_provision.sh /usr/local/bin

RUN bash env_provision.sh && \
    # install composer
    curl -sSk https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && rm -rf /var/lib/apt/lists/* \
    && touch /etc/apache2/conf-available/fqdn.conf && a2enconf fqdn && a2enmod rewrite \
    # install nodejs and yarn
    && curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh \
    && bash nodesource_setup.sh \
    && apt-get install nodejs -y \
    && npm install npm@latest -g \
    && npm install -g yarn@$YARN_VERSION \
   # Install PHP Redis extension
    && pecl install -o -f redis \
    &&  rm -rf /tmp/pear 

WORKDIR /var/www/app/



EXPOSE 80

CMD ["./docker/entrypoint.sh"]