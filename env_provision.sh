#!/bin/bash

set -xe
apt-get update -yqq
apt-get upgrade -yqq

apt-get -y install apt-transport-https \
           ca-certificates \
           curl lsb-release \
           wget

wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
sh -c 'echo "deb https://packages.sury.org/php/ stretch main" > /etc/apt/sources.list.d/php.list'

set env PHP_VERSION_FULL=php7.4


apt-get update -yqq && apt-get install -yqq --no-install-recommends \
        apt-utils \
        apache2 \
        acl \
        build-essential \
        curl \
        gnupg \
        php-pear \
        $PHP_VERSION_FULL-dev \
        make \
        gnupg2 \
        git \
        locales \
        libzip-dev \
        libxslt-dev \
        mailutils \
        nodejs \
        net-tools \
        $PHP_VERSION_FULL \
        $PHP_VERSION_FULL-cli \
        $PHP_VERSION_FULL-common \
        $PHP_VERSION_FULL-curl \
        $PHP_VERSION_FULL-mbstring \
        $PHP_VERSION_FULL-mysql \
        $PHP_VERSION_FULL-xml \
        $PHP_VERSION_FULL-zip \
        $PHP_VERSION_FULL-soap \
        php-xdebug \
        unzip \
        vim \
        zlib1g-dev \
        zip \
        wkhtmltopdf
