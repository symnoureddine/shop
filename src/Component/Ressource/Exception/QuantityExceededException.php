<?php

declare(strict_types=1);

namespace App\Component\Ressource\Exception;

use Exception;

class QuantityExceededException extends Exception
{
    protected $message = 'You have addes the max stock for this itel.';
}
