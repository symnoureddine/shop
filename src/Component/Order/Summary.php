<?php

declare(strict_types=1);

namespace App\Component\Order;

use App\Component\Order\Model\OrderInterface;

final class Summary
{
    /**
     * @var OrderInterface
     */
    private $order;

    /**
     * Summary constructor.
     */
    public function __construct(OrderInterface $order)
    {
        $this->order = $order;
    }

    /**
     * Return.
     */
    public function getItemsPriceTotal(): float
    {
        return $this->order->getItemsPriceTotal();
    }

    /**
     * Return.
     */
    public function getPriceTotal(): float
    {
        return $this->order->getPriceTotal();
    }
}
