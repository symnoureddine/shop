<?php

declare(strict_types=1);

namespace App\Component\Order\Factory;

use App\Component\Order\Events;
use App\Component\Order\Model\OrderInterface;
use App\Component\Order\Model\OrderItem;
use App\Component\Order\Model\OrderItemInterface;
use App\Component\Order\Storage\OrderSessionStorage;
use App\Component\Order\Summary;
use App\Component\Product\Model\ProductInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\Security;

class OrderFactory implements OrderFactoryInterface
{
    /** @var OrderInterface */
    private $factory;

    /** @var OrderSessionStorage */
    private $storage;

    /** @var OrderInterface */
    private $order;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /** @var Security */
    private $security;

    public function __construct(
        OrderInterface $factory,
        OrderSessionStorage $storage,
        EntityManagerInterface $entityManager,
        EventDispatcherInterface $eventDispatcher,
        Security $security
    ) {
        $this->factory = $factory;
        $this->storage = $storage;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->security = $security;
        $this->order = $this->getCurrent();
    }

    /**
     * {@inheritdoc}
     */
    public function createNew(): OrderInterface
    {
        return new $this->factory();
    }

    public function getCurrent(): OrderInterface
    {
        $order = $this->storage->getOrderById();
        if (null !== $order) {
            return $order;
        }

        return $this->createNew();
    }

    public function addItem(ProductInterface $product, int $quantity): void
    {
        $orderBeforeId = $this->order->getId();
        if (!$this->containsProduct($product)) {
            $orderItem = new OrderItem();
            $orderItem->setOrder($this->order);
            $orderItem->setProduct($product);
            $orderItem->setQuantity($quantity);

            $this->order->addItem($orderItem);
        } else {
            $key = $this->indexOfProduct($product);
            $item = $this->order->getItems()->get($key);
            $quantity = $item->getQuantity() + $quantity;
            $this->setItemQuantity($item, $quantity);
        }
        $this->order->setUser($this->security->getUser());

        $this->entityManager->persist($this->order);

        if (null === $orderBeforeId) {
            $event = new GenericEvent($this->order);
            $this->eventDispatcher->dispatch($event, Events::ORDER_CREATED);
        } else {
            $event = new GenericEvent($this->order);
            $this->eventDispatcher->dispatch($event, Events::ORDER_UPDATED);
        }

        $this->entityManager->flush();
    }

    public function containsProduct(ProductInterface $product): bool
    {
        foreach ($this->order->getItems() as $item) {
            if ($item->getProduct() === $product) {
                return true;
            }
        }

        return false;
    }

    public function indexOfProduct(ProductInterface $product): ?int
    {
        foreach ($this->order->getItems() as $key => $item) {
            if ($item->getProduct() === $product) {
                return $key;
            }
        }

        return null;
    }

    public function setItemQuantity(OrderItemInterface $item, int $quantity): void
    {
        if ($this->order && $this->order->getItems()->contains($item)) {
            $key = $this->order->getItems()->indexOf($item);

            $item->setQuantity($quantity);

            $this->order->getItems()->set($key, $item);

            // Run events
            $event = new GenericEvent($this->order);
            $this->eventDispatcher->dispatch($event, Events::ORDER_UPDATED);

            $this->entityManager->persist($this->order);
            $this->entityManager->flush();
        }
    }

    public function removeItem(OrderItemInterface $item): void
    {
        if ($this->order && $this->order->getItems()->contains($item)) {
            $this->order->removeItem($item);

            $event = new GenericEvent($this->order);
            $this->eventDispatcher->dispatch($event, Events::ORDER_UPDATED);

            $this->entityManager->persist($this->order);
            $this->entityManager->flush();
        }
    }

    public function items(): Collection
    {
        return $this->order->getItems();
    }

    public function clear(): void
    {
        $this->entityManager->remove($this->order);
        $this->entityManager->flush();
    }

    public function isEmpty(): bool
    {
        return !$this->order->getItems();
    }

    public function summary(): Summary
    {
        return new Summary($this->order);
    }
}
