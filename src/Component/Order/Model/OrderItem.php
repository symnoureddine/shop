<?php

declare(strict_types=1);

namespace App\Component\Order\Model;

use App\Component\Core\Model\TimestampableTrait;
use App\Component\Product\Model\Product;
use App\Component\Product\Model\ProductInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="`order_item`")
 */
class OrderItem implements OrderItemInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="guid", unique=true)
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="items")
     * @ORM\JoinColumn(nullable=false ,onDelete="CASCADE")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="orderItem")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="integer",nullable= false)
     */
    private $quantity;

    /**
     * @ORM\Column(type="float",nullable= false, options={"default" : 0.00})
     */
    private $priceTotal = 0;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getOrder(): OrderInterface
    {
        return $this->order;
    }

    public function setOrder(OrderInterface $order): void
    {
        $this->order = $order;
    }

    public function getProduct(): ProductInterface
    {
        return $this->product;
    }

    public function setProduct(ProductInterface $product): void
    {
        $this->product = $product;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    public function getPriceTotal(): float
    {
        return $this->priceTotal;
    }

    public function setPriceTotal(float $priceTotal): void
    {
        $this->priceTotal = $priceTotal;
    }
}
