<?php

declare(strict_types=1);

namespace App\Component\Order\Model;

use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

interface OrderInterface
{
    /**
     * @return string
     */
    public function getId(): ?string;

    public function addItem(OrderItemInterface $item): void;

    public function removeItem(OrderItemInterface $item): void;

    public function getItems(): Collection;

    public function setItemsTotal(int $itemsTotal): void;

    public function setItemsPriceTotal(float $itemsPriceTotal): void;

    /**
     * Total price of products including the discount.
     */
    public function getItemsPriceTotal(): float;

    public function getItemsTotal(): int;

    public function setPriceTotal(float $priceTotal): void;

    public function getPriceTotal(): float;

    public function setUser(?UserInterface $user): void;

    public function getUser(): ?UserInterface;
}
