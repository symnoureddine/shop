<?php

declare(strict_types=1);

namespace App\Component\Order\Model;

use App\Component\Product\Model\ProductInterface;

interface OrderItemInterface
{
    /**
     * @return string
     */
    public function getId(): ?string;

    public function setOrder(OrderInterface $order): void;

    public function getOrder(): OrderInterface;

    public function setProduct(ProductInterface $product): void;

    public function getProduct(): ProductInterface;

    public function setQuantity(int $quantity): void;

    public function getQuantity(): int;

    public function setPriceTotal(float $priceTotal): void;

    public function getPriceTotal(): float;
}
