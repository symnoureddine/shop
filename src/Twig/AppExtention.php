<?php

declare(strict_types=1);

namespace App\Twig;

use App\Component\Order\Storage\OrderSessionStorage;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtention extends AbstractExtension
{
    /** @var OrderSessionStorage */
    private $storage;

    public function __construct(OrderSessionStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('getItemsTotal', [$this, 'getItemsTotal']),
        ];
    }

    public function getItemsTotal(): ?int
    {
        $order = $this->storage->getOrderById();

        return $order ? $order->getItemsTotal() : null;
    }
}
