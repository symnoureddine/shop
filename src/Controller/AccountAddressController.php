<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\Address\Model\Address;
use App\Form\AddressType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

/**
 * @Route("/account")
 */
class AccountAddressController extends AbstractController
{
    private $entityManager;

    private $breadcrumbs;

    public function __construct(EntityManagerInterface $entityManager, Breadcrumbs $breadcrumbs)
    {
        $this->entityManager = $entityManager;
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * @Route("/address", name="account_address")
     */
    public function index(): Response
    {
        $this->breadcrumbs->addItem('Home', $this->get('router')->generate('home'));
        $this->breadcrumbs->addItem('My account', $this->get('router')->generate('account'));
        $this->breadcrumbs->addItem('Address book', $this->get('router')->generate('account_address'));

        return $this->render('address/index.html.twig', []);
    }

    /**
     * @Route("/add",name="account_address_add")
     */
    public function addAddress(Request $request): Response
    {
        $address = new Address();

        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $address->setUser($this->getUser());
            $this->entityManager->persist($address);
            $this->entityManager->flush();

            return $this->redirectToRoute('account_address');
        }

        return $this->render('address/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}",name="account_address_edit")
     */
    public function editAddress(Request $request, Address $address): Response
    {
        if (!$address || $address->getUser() !== $this->getUser()) {
            return $this->redirectToRoute('account_address');
        }

        $form = $this->createForm(AddressType::class, $address);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('account_address');
        }

        $form = $this->createForm(AddressType::class, $address);

        return $this->render('address/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/supprimer-une-adresse/{id}", name="account_address_delete")
     */
    public function delete(Request $request, Address $address): Response
    {
        if (!$address) {
            throw $this->createNotFoundException();
        }
        if ($request->isXmlHttpRequest()) {
            if ($address && $address->getUser() === $this->getUser()) {
                $this->entityManager->remove($address);
                $this->entityManager->flush();
            }
        }

        return new Response($this->render('address/delete_successfull.html.twig', ['user' => $this->getUser()]));
    }
}
