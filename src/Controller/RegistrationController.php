<?php

declare(strict_types=1);

namespace App\Controller;

use App\Component\User\Model\User;
use App\Form\RegistrationFormType;
use App\Utils\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractController
{
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    /** @var MailerService */
    private $mailerService;

    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        MailerService $mailerService
    ) {
        $this->translator = $translator;
        $this->entityManager = $entityManager;
        $this->mailerService = $mailerService;
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $searchEmail = $this->entityManager->getRepository(User::class)->findOneByEmail($user->getEmail());

            if (!$searchEmail) {
                // encode the plain password
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $user->getPassword()
                    )
                );

                $this->entityManager->persist($user);
                $this->entityManager->flush();

                $content = 'Bonjour '.$user->getFirstname().
                           '<br/>Bienvenue sur la première boutique dédiée au made in France.<br><br/>'.
                           'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'.
                           'Aperiam expedita fugiat ipsa magnam mollitia optio voluptas! Alias,'.
                           'aliquid dicta ducimus exercitationem facilis, incidunt magni,'.
                           'minus natus nihil odio quos sunt?';

                $mail = $this->mailerService->create(
                    'emails/register.html.twig',
                    $user->getEmail(),
                    [
                        'mail' => $user->getEmail(),
                        'content' => $content,
                    ]
                );

                $this->mailerService->send($mail);

                $this->translator->trans('app.user.register.message.success');
                $this->addFlash(
                    'success',
                    'Votre inscription s\'est correctement déroulée.'.
                    'Vous pouvez dès à présent vous connecter à votre compte.'
                );
            } else {
                $this->translator->trans('app.user.register.message.warning');
                $this->addFlash('warning', 'L\'email que vous avez renseigné existe déjà.');
            }
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }
}
