<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\UserChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class AccountController extends AbstractController
{
    private $entityManager;

    private $breadcrumbs;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * AccountController constructor.
     *
     * @param $entityManager
     * @param $breadcrumbs
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Breadcrumbs $breadcrumbs,
        TranslatorInterface $translator
    ) {
        $this->entityManager = $entityManager;
        $this->breadcrumbs = $breadcrumbs;
        $this->translator = $translator;
    }

    /**
     * @Route("/account/dashboard", name="account")
     */
    public function dashboard(): Response
    {
        $this->breadcrumbs->addItem('Home', $this->get('router')->generate('home'));
        $this->breadcrumbs->addItem('My account', $this->get('router')->generate('account'));

        return $this->render('account/dashboard.html.twig');
    }

    /**
     * @Route("account/profile/edit", name="profile")
     */
    public function profile(): Response
    {
        $this->breadcrumbs->addItem('Home', $this->get('router')->generate('home'));
        $this->breadcrumbs->addItem('My account', $this->get('router')->generate('account'));
        $this->breadcrumbs->addItem('Personal information', $this->get('router')->generate('profile'));

        return $this->render('account/profile.html.twig');
    }

    /**
     * @Route("/account/change-password", name="account_change_password")
     */
    public function changePasswordAction(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        $this->breadcrumbs->addItem('Home', $this->get('router')->generate('home'));
        $this->breadcrumbs->addItem('My account', $this->get('router')->generate('account'));
        $this->breadcrumbs->addItem('Change password', $this->get('router')->generate('account'));

        $user = $this->getUser();

        $form = $this->createForm(UserChangePasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $currentPassword = $form->get('currentPassword')->getData();
            if ($encoder->isPasswordValid($user, $currentPassword)) {
                $newPwd = $form->get('newPassword')->getData();
                $password = $encoder->encodePassword($user, $newPwd);

                $user->setPassword($password);
                $this->entityManager->flush();
                $this->translator->trans('app.account.rest.password.message.success');
                $this->addFlash('success', 'Votre mot de passe a bien été mis à jour.');
            } else {
                $this->translator->trans('app.account.rest.password.message.error');
                $this->addFlash('error', 'Votre mot de passe actuel n\'est pas le bon');
            }
        }

        return $this->render('account/changePassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
