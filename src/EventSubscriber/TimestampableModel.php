<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Doctrine\ORM\Event\LifecycleEventArgs;

class TimestampableModel
{
    /**
     * Add createdAt timestamp.
     */
    public function prePersist(LifeCycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (true === $this->hasTimestampableModelCreateTrait($entity)) {
            $entity->setCreatedAt(new \DateTime());
        }
    }

    private function hasTimestampableModelCreateTrait($entity)
    {
        if (\array_key_exists('App\Component\Core\Model\TimestampableTrait', class_uses($entity))) {
            return true;
        }

        return false;
    }

    /**
     * Add updatedAt timestamp.
     */
    public function preUpdate(LifeCycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if (true === $this->hasTimestampableModelUpdateTrait($entity)) {
            $entity->setUpdatedAt(new \DateTime());
        }
    }

    private function hasTimestampableModelUpdateTrait($entity)
    {
        if (\array_key_exists('App\Component\Core\Model\TimestampableTrait', class_uses($entity))) {
            return true;
        }

        return false;
    }
}
