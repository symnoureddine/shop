<?php

declare(strict_types=1);

namespace App\Utils;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

class MailerService
{
    private $mailer;

    private $twig;

    private $replyTo;

    private $newTemplatedEmail;

    private $createAddress;

    public function __construct(
        Environment $twig,
        MailerInterface $mailer,
        string $replyTo,
        string $mailFrom,
        string $mailSenderName
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->replyTo = $replyTo;
        $this->newTemplatedEmail = new TemplatedEmail();
        $this->createAddress = new Address($mailFrom, $mailSenderName);
    }

    /**
     * @param array<string, mixed> $context
     */
    public function create(string $template, string $recipients, array $context): TemplatedEmail
    {
        $subject = $this->getSubject($template);

        return $this->newTemplatedEmail
            ->from($this->createAddress)
            ->replyTo($this->replyTo)
            ->to($recipients)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($context);
    }

    public function send(TemplatedEmail $message): void
    {
        $this->mailer->send($message);
    }

    public function getSubject(string $template): string
    {
        $template = $this->twig->load($template);

        return $template->renderBlock('subject');
    }
}
