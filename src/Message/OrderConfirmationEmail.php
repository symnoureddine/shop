<?php

declare(strict_types=1);

namespace App\Message;

class OrderConfirmationEmail
{
    private $orderId;

    public function __construct(string $orderId)
    {
        $this->orderId = $orderId;
    }

    public function getOrderId(): string
    {
        return $this->orderId;
    }
}
